# Tvorba fantasy ilustrací pomocí generativních neuronových sítí.

Semestrální práce pro předmět NI-MVI

## Index

[Stav práce]()

[Popis úkolu](https://gitlab.fit.cvut.cz/sofrondr/mvi-sp/blob/master/README.md#popis-%C3%BAkolu)

[Popis použitých dat](https://gitlab.fit.cvut.cz/sofrondr/mvi-sp/edit/master/README.md#popis-pou%C5%BEit%C3%BDch-dat)

[Popis přiložených dat a návod na zisk kompletního datasetu](https://gitlab.fit.cvut.cz/sofrondr/mvi-sp/edit/master/README.md#popis-p%C5%99ilo%C5%BEen%C3%BDch-dat-a-n%C3%A1vod-na-zisk-kompletn%C3%ADho-datasetu)

## Stav práce

Práce je připravena na odevzdání, ale bohužel jsem nestihl všechny experimenty, které jsem stihnout chtěl, obzvláště tvorbu GAN modelů

## Popis úkolu

Pro mnohé účely (například ilustrace knih, deskových her nebo hudebních alb) je potřeba kvalitních ilustrací s fantasy tématikou. Ačkoliv se jejich tvorbou zabývá mnoho umělců, tak se jedná o drahou cestu, jak originální ilustrace získat. Mohla by v tomto ohledu alespoň částečně nahradit lidskou kreativitu umělá inteligence?

Cílem práce je natrénovat několik modelů a porovnat jejich úspěšnost při tvorbě fantasy ilustrací. V základní variantě libovolných, v rozšířené variantě poté ilustrací, určených pomocí jednoduché mechaniky výběru klíčových slov popisujících jejich obsah. 

## Popis použitých dat

Pro trénink modelů je použit dataset fantasy ilustrací, které pochází z karetní hry Magic the Gathering. Získány byly z webové stránky www.scryfall.com, která shromažďuje informace o všech vydaných kartách v historii této hry. Na této stránce jsou k dispozici bohatá metadata popisující například funkci karet v rámci hry nebo okolnosti jejich vydání. Hlavně ale jsou zde skeny karet ve vysokém rozlišení, které je možné pro nekomerční užití používat. 

Provedl jsem vyfiltorvání karet obsahujících reprezentativní ilustrace a metadata a získání výřezů ze skenů karet takových, že zabírají pouze samotnou ilustraci a nikoliv pro tuto úlohu nedůležité části jako je rámeček a pravidla a schopnosti karet. Výsledkem je dataset obsahující zhruba 15 000 ilustrací a u každé ještě obsáhlá metadata příslušné karty. Data jsou rozdělena do tří částí dle vizuální identity, která v průběhu let dostála několika výrazných změn. Jedná se o vzhled zavedený v roce 1997 (cca 3 000 karet), přepracovaný v roce 2003 (cca 5000 karet) a poté v roce 2015 (cca 7000 karet) - z hlediska úlohy však lze trénovat s všemi daty dohromady, důležité je toto dělení jen pro předzpracování. Původní obrázky ve formátu .jpg lze nalézt ve složce /data případně v rámci výběru v složce /sample_data. Jelikož se jedná o skeny natisknutých karet, má většina těchto obrázků nedostatky, co se týče šumu vzniklého tiskařskou a skenovací technologií. Proto je bylo potřeba vyhladit v rámci předzpracování. Dále jsou obrázky dostupné v daných složkách ve verzích s menšími rozměry tak, aby byly vhodnější pro trénink neuronových sítí na omezených zdrojích.

### Podrobnější popis procesu získávání a předzpracování dat

Popis předzpracování lze nalézt v přiloženém reportu

Skripty pro scraping databáze karet, ilustrací a jejich obrazové předzpracování lze nalézt v adresáři /data_preprocessing

## Popis přiložených dat a návod na zisk kompletního datasetu

Kompletní dataset obsahující 15 000 skenů je příliš rozsáhlý pro ukládání na Gitu, nicméně práce obsahuje veškeré skripty pro jeho stažení a předzpracování do stejné podoby, v jaké jsou data použita. Ty lze najít v adresáři /data_preprocessing, kdy je nutné nejprve spustit skript card_list_fetcher.ipynb, poté art_fecther.ipynb a nakonec image_preprocessing.ipynb.

V tomto repozitáři jsou nicméně pro základní průzkum dat připraveny odpovídající adresáře sdružené do složky /sample_data, které obsahují jen desítky ilustrací a informací o kartách.

## Struktura repozitáře

/data_preprocessing je adresář obsahující skripty pro stažení kompletních dat

/models je adresář obsahující Python notebooky s kompletním kódem nutným ke trénování modelů a zobrazení jejich výsledků. Obsahuje také uložené předtrénované modely

/report je složka s soubory potřebnými pro kompilaci reportu
